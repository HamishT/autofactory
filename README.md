# Autofactory #

### Status: hiatus (considering retry in another language) ###

A casual side project based on the manufacturing simulation game Factorio: this was to be an assistant program to help with floor planning and production ratios. I find Java a bit unwieldy for a task like this (mostly graph data processing) so it's mostly here to demonstrate Java 8 competency.