import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hamish on 20/09/2017.
 */
public class ItemPool {

    public ItemPool() {
        //empty
    }

    private List<String> items = new ArrayList<>();

    int getByName(String name) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).equals(name)) {
                return i;
            }
        }
        int tail = items.size();
        items.add(name);
        return tail;
    };

    String getById(int id) {
        if (id >= items.size()) {
            throw new IllegalArgumentException("id exceeds known number of items");
        }
        return items.get(id);
    }

}
