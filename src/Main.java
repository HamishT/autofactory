import java.io.File;
import java.util.stream.Stream;

/**
 * Created by Hamish on 20/09/2017.
 */
public class Main {

    private static class Pair {
        private String a; private double b;
        Pair(String a, double b) {
            this.a = a; this.b = b;
        }
        String fst() { return a; }
        double snd() { return b; }
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Usage: LuaParser <file>");
            System.exit(0);
        }

        System.out.println("Initialising pools...");
        ItemPool ip = new ItemPool();
        RecipePool rp = new RecipePool();

        System.out.println("Initialising parser...");
        File f = new File(args[0]);
        RecipeDumpParser parser = new RecipeDumpParser(ip, rp, f);

        System.out.println("Running parser...");
        parser.parse();

        System.out.println("Initialising graph...");
        BasicGraph g = new BasicGraph(ip, rp);

        System.out.println("Adding sample base products to graph (oil products, iron plate, copper plate)...");
//        Stream.of("heavy-oil", "light-oil", "petroleum-gas", "iron-plate", "copper-plate")
//                .mapToInt(ip::getByName)
//                .forEach(g::addFundamental);

        System.out.println("Adding sample supply demands to graph...");

        Stream.of(
            // win condition (5-minute rocket construction)
            new Pair("satellite", 1d/300),
            new Pair("rocket-part", 100d/300),
            // science packs
            new Pair("science-pack-1", 2),
            new Pair("science-pack-2", 2),
            new Pair("science-pack-3", 2),
            new Pair("military-science-pack", 2),
            new Pair("production-science-pack", 2),
            new Pair("high-tech-science-pack", 2),
            // belts
            new Pair("express-transport-belt", 5),
            new Pair("express-underground-belt", 1),
            new Pair("express-splitter", 1),
            // robots
            new Pair("logistic-robot", 1),
            new Pair("construction-robot", 1),
            // inserters
            new Pair("fast-inserter", 1),
            new Pair("long-handed-inserter", 1),
            new Pair("filter-inserter", 1),
            new Pair("stack-inserter", 1),
            new Pair("stack-filter-inserter", 1),
            // assembly and refinery
            new Pair("assembling-machine-3", 0.5),
            new Pair("electric-furnace", 0.5)
        ).forEach(p -> g.addProduct(ip.getByName(p.fst()), p.snd()));

        System.out.println("Building graph...");
        g.buildGraph();

        System.out.println("Printing graph...");
        System.out.println(g.toString());
    }

}
