/**
 * Created by Hamish on 22/09/2017.
 */
public enum ProductType {

    UNIQUE_RECIPE,
    SHARED_RECIPE,
    NO_RECIPE

}
