import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Hamish on 20/09/2017.
 */
public class RecipePool {

    private Map<Integer, List<Recipe>> outputToRecipeList = new HashMap<>();

    public void addRecipe(Recipe r) {
        r.getOutputs().keySet().forEach(i -> {
            outputToRecipeList.putIfAbsent(i, new ArrayList<>());
            outputToRecipeList.get(i).add(r);
        });
    }

    public List<Recipe> getRecipes(int output) {
        if (!outputToRecipeList.containsKey(output)) {
            return new ArrayList<>(); // empty
        }
        return outputToRecipeList.get(output);
    }

}
