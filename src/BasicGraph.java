import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by Hamish on 20/09/2017.
 */
public class BasicGraph {

    private static class Node {
        enum RecipeType {
            asdas;
        }
        Recipe recipe = null;
        int outputItem = -1;
        double outputCount = 0; // valid if UNIQUE_RECIPE
        ProductType productType = null;
        // graph only records outbound edges now (may redesign later)
        Map<Integer, Edge> inEdges = new HashMap<>();
        Map<Integer, Edge> outEdges = new HashMap<>();
        double surplusOutput = 0;
    }

    private static class Edge {
        Node node0, node1;
        double amount;

        Edge(Node n0, Node n1, double amount) {
            node0 = n0;
            node1 = n1;
            this.amount = amount;
        }
    }

    private ItemPool itemPool;
    private RecipePool recipePool;

    private List<Integer> ordering = new ArrayList<>();
    private Map<Integer, Double> products = new HashMap<>();
    private Set<Integer> fundamentals = new HashSet<>();
    private Map<Integer, Node> nodes = new HashMap<>();

    public BasicGraph(ItemPool itemPool, RecipePool recipePool) {
        this.itemPool = itemPool;
        this.recipePool = recipePool;
    }

    public void addProduct(int item, double count) {
        products.merge(item, count, Double::sum);
    }

    public void addProductByName(String name, double count) {
        addProduct(itemPool.getByName(name), count);
    }

    public void addFundamental(int item) {
        fundamentals.add(item);
    }

    public void buildGraph() {
        buildGraphNodes();
        buildGraphEdges();
        generateOrdering();
    }

    private void buildGraphNodes() {
        Deque<Integer> queue = new ArrayDeque<>(products.keySet());
        while (!queue.isEmpty()) {

            // Pop the item queue
            int itemNo = queue.removeFirst();

            assert !nodes.containsKey(itemNo);

            // Create a new node
            // Since these are created from the keys of products, there cannot be duplicates
            Node n = new Node();
            nodes.put(itemNo, n);
            n.outputItem = itemNo;
            List<Recipe> rs = recipePool.getRecipes(itemNo);
            if (rs.size() == 0 || fundamentals.contains(itemNo)) {
                n.recipe = null;
                n.productType = ProductType.NO_RECIPE;
            } else {
                Recipe r0 = rs.get(0);
                if (rs.size() > 1 || r0.getOutputs().size() > 1) {
                    n.recipe = null;
                    n.productType = ProductType.SHARED_RECIPE;
                } else {
                    n.recipe = r0;
                    // hmm...
                    n.outputCount = r0.getOutputs().values().stream().mapToDouble(d -> d).sum();
                    n.productType = ProductType.UNIQUE_RECIPE;
                }
            }
            n.surplusOutput = products.getOrDefault(itemNo, 0d);

            // Queue inputs if they are absent from graph
            if (n.recipe != null) {
                n.recipe.getInputs().keySet().forEach(inputItemNo -> {
                    if (!nodes.containsKey(inputItemNo) && !queue.contains(inputItemNo)) {
                        queue.addLast(inputItemNo);
                    }
                });
            }
        }
    }

    private void buildGraphEdges() {
        Deque<Integer> queue = new ArrayDeque<>(products.keySet());

        while (!queue.isEmpty()) {
            int itemDst = queue.removeFirst();

            Node nodeDst = nodes.get(itemDst);
            Recipe recipeDst = nodeDst.recipe;
            // required raw output
            if (recipeDst != null) {
                double requiredOut = nodeDst.outEdges.values().stream().mapToDouble(e -> e.amount).sum() + nodeDst.surplusOutput;
                // normalised to recipe result count, e.g. if recipe produces 2*item, only need half as much input to meet raw output
                double normalisedOut = requiredOut / nodeDst.outputCount;

                recipeDst.getInputs().forEach((itemSrc, cntSrc) -> {

                    // Note this might replace an existing edge: this is intended
                    Node nodeSrc = nodes.get(itemSrc);
                    double requiredIn = cntSrc * normalisedOut;
                    Edge e = new Edge(nodeSrc, nodeDst, requiredIn);
                    nodeDst.inEdges.put(itemSrc, e);
                    nodeSrc.outEdges.put(itemDst, e);

                    if (!queue.contains(itemSrc)) {
                        queue.addLast(itemSrc);
                    }
                });
            }
        }
    }

    private void generateOrdering() {
        ordering = new ArrayList<>();
        Set<Edge> seenEdges = new HashSet<>();
        Deque<Integer> q = new ArrayDeque<>();
        nodes.entrySet().stream()
                .filter(e -> e.getValue().inEdges.isEmpty())
                .map(Map.Entry::getKey)
                .forEach(q::addLast);

        while (!q.isEmpty()) {
            int item = q.removeFirst();
            ordering.add(item);
            Node n0 = nodes.get(item);
            n0.outEdges.forEach((outItem, outEdge) -> {
                if (!seenEdges.contains(outEdge)) {
                    seenEdges.add(outEdge);
                    Node n1 = outEdge.node1;
                    if (n1.inEdges.values().stream().allMatch(seenEdges::contains)) {
                        q.addLast(outItem);
                    }
                }
            });
        }

//        toposortDFS();
    }

//    private void toposortDFS() {
//        Set<Integer> unmarked = new HashSet<>(nodes.keySet());
//        while (!unmarked.isEmpty()) {
//            int item = unmarked.iterator().next();
//            topologicalVisit(item, unmarked);
//        }
//    }
//
//    private void topologicalVisit(int item, Set<Integer> unmarked) {
//        if (!unmarked.contains(item))
//            return;
//        nodes.get(item).outEdges.keySet().forEach(i -> topologicalVisit(i, unmarked));
//        unmarked.remove(item);
//        ordering.add(0, item);
//    }

    @Override
    public String toString() {

        String preProd = String.format("products = {%n");
        String preNode = String.format("nodes = {%n");
        String delim = String.format(",%n");
        String suffix = String.format("%n}%n");
        String showProd = products.entrySet().stream()
                .map(e -> String.format("    {%s x %.3f", itemPool.getById(e.getKey()), e.getValue()))
                .collect(Collectors.joining(delim, preProd, suffix));
        String showNodes = ordering.stream()
                .map(nodes::get)
                .map(this::toNodeString)
                .map("    "::concat)
                .collect(Collectors.joining(delim, preNode, suffix));
        return showProd.concat(showNodes);
    }

    private String toNodeString(Node n) {
        StringJoiner sj = new StringJoiner(", ", "{", "}");
        //sj.add("id=" + n.outputItem);
        double supply = n.outEdges.values().stream().mapToDouble(e -> e.amount).sum();
        double surplus = n.surplusOutput;
        double total = supply + surplus;

        String svolume = (n.recipe != null) ? String.format("volume=%.1f", total*n.recipe.time/n.outputCount) : "raw";

        sj.add("'" + itemPool.getById(n.outputItem) + "'");
        sj.add(svolume);
        if (surplus > 0) sj.add(String.format("surplus=%.1f", surplus));
        sj.add(String.format("supply=%.1f", supply));
        n.outEdges.values().forEach(e ->
                sj.add(String.format("%.1f->%s", e.amount, itemPool.getById(e.node1.outputItem))));
        return sj.toString();
    }

}
