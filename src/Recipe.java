import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Hamish on 20/09/2017.
 */
public class Recipe {

    public String name;
    public String category;
    public double time = 1;
    private Map<Integer, Double> inputs = new HashMap<>();
    private Map<Integer, Double> outputs = new HashMap<>();
    // currently only supports single-product recipes

    public Recipe() {
        //empty
    }

    public void addInput(int ingr, double count) {
        inputs.merge(ingr, count, Double::sum);
    }

    public void addOutput(int ingr, double count) {
        outputs.merge(ingr, count, Double::sum);
    }

    public Map<Integer, Double> getInputs() {
        return Collections.unmodifiableMap(inputs);
    }

    public Map<Integer, Double> getOutputs() {
        return Collections.unmodifiableMap(outputs);
    }

}
