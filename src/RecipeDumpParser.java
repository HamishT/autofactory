import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Hamish on 21/09/2017.
 */
public class RecipeDumpParser {

    private static final boolean BLACKLIST_BARRELS = true;

    private ItemPool itemPool;
    private RecipePool recipePool;

    private List<String> lines = new ArrayList<>();

    private Recipe currRecipe = null;

    public RecipeDumpParser(ItemPool itemPool, RecipePool recipePool, File dumpFile) {
        this.itemPool = itemPool;
        this.recipePool = recipePool;
        try {
            lines = Files.readAllLines(dumpFile.toPath());
        } catch (IOException ioe) {
            ioe.printStackTrace();
            System.exit(1);
        }
    }

    public void parse() {

        // Parse every line for recipe data

        for (String line: lines) {
            String[] split0 = line.split(",", 2);
            String cmd = split0[0];
            Map<String, String> attr = null;
            if (split0.length == 2) {
                String tokenString = split0[1];
                attr = Arrays.stream(tokenString.split(","))
                        .map(s -> s.split("="))
                        .collect(Collectors.toMap(ss -> ss[0], ss -> ss[1]));
            }

            if (cmd.equals("recipe")) {
                String name = attr.get("name");
                if (!(BLACKLIST_BARRELS && name.contains("barrel"))) {
                    currRecipe = new Recipe();
                    currRecipe.name = attr.get("name");
                    currRecipe.category = attr.get("category");
                    currRecipe.time = Double.parseDouble(attr.get("energy"));
                } else {
                    currRecipe = null;
                }
            }
            else if (cmd.equals("ingredient")) {
                if (currRecipe != null) {
                    currRecipe.addInput(itemPool.getByName(attr.get("name")), Integer.parseInt(attr.get("amount")));
                }
            }
            else if (cmd.equals("product")) {
                if (currRecipe != null) {
                    int itemNo = itemPool.getByName(attr.get("name"));
                    if (attr.containsKey("amount")) {
                        currRecipe.addOutput(itemNo, Double.parseDouble(attr.get("amount")));
                    } else {
                        int min = Integer.parseInt(attr.get("min"));
                        int max = Integer.parseInt(attr.get("max"));
                        if (min != max) {
                            System.err.println("Encountered probabilistic-amount recipe, ignoring...");
                            currRecipe = null;
                        } else {
                            double amount = max * Double.parseDouble(attr.get("prob"));
                            currRecipe.addOutput(itemPool.getByName(attr.get("name")), amount);
                        }
                    }
                }
            }
            else if (cmd.equals("end_recipe")) {
                if (currRecipe != null) {
                    recipePool.addRecipe(currRecipe);
                }
            }
            else {
                System.err.println("Unrecognised line start token '" + cmd + "'.");
                System.exit(1);
            }
        }
    }

    //

}
