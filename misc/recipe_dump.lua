function flog (m)
    game.write_file("recipe-data.txt", m .. "\r\n", true);
end

game.remove_path("recipe_data.txt");
for _,v in pairs(game.player.force.recipes) do
    local pr = v.prototype;
    if (pr.valid) then
        flog("recipe,name=" .. pr.name .. ",category=" .. pr.category .. ",energy=" .. pr.energy);
        for _,i in ipairs(pr.ingredients) do
            flog("ingredient,type=" .. i["type"] .. ",name=" .. i["name"] .. ",amount=" .. i["amount"]);
        end
        for _,p in ipairs(pr.products) do
            if (p["amount"] == nil) then
                flog("product,type=" .. p["type"] .. ",name=" .. p["name"] .. ",min=" .. p["amount_min"] .. ",max=" .. p["amount_max"] .. ",prob=" .. p["probability"])
            else
                flog("product,type=" .. p["type"] .. ",name=" .. p["name"] ..  ",amount=" .. p["amount"]);
            end
        end
        flog("end_recipe");
    end
end